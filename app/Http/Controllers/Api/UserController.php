<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use function Symfony\Component\Console\Tests\Command\createClosure;
use Validator;

class UserController extends Controller
{

    public $successStatus = 200;


    public function register(Request $request){

        $input = $request->all();
        $validator = Validator::make($input,[
            'name' => 'required|min:5',
            'email' => 'required|email|unique:users',
            'password' => 'required|alpha_num|min:5'
        ]);

        if($validator->fails()){

            return response($validator->errors());

        }

//


        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        return response($success);

    }


//    login api
    public function login(Request $request){


        $input = $request->all();

        $validator = Validator::make($input,[
            'email' => 'required|email',
            'password' => 'required|min:5|max:10'
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->errors()], 401);
        }

        if (Auth::attempt(['email' => $input['email'] ,'password'=> $input['password'] ])){
            $user = Auth::user();

            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response($success);

        }else {

            return response()->json('Wrong Password or Login');

        }

    }


    public function showUser()
    {

        $user = Auth::user();
        return response( $user);
    }




}
