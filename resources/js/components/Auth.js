import React from 'react';
import {  Cookies } from 'react-cookie';

    const Auth = {
        cookies: new Cookies(),
        isAuthenticated: false,
        isAuth: function() {
            if (this.cookies.get('user') !== undefined) {
                this.isAuthenticated = true;
                return true
            }
            return false;
        }
    };

export default Auth;