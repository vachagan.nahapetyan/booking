import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import {Link} from "react-router-dom";
import {Cookies} from 'react-cookie';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";






class Registration extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            nameError: false,
            emailError: false,
            passwordError: false,
            nameErrorMsg: '',
            emailErrorMsg: '',
            passwordErrorMsg: '',
            loading: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validate = this.validate.bind(this);
    }


    handleChange(event) {
        const data = {};
        data[event.target.name] = event.target.value;

        this.setState(
            data
        );

    };

    //
    validate() {

        let Error = false;
        const errors = {
            nameError: false,
            nameErrorMsg: '',
            emailError: false,
            emailErrorMsg: '',
            passwordError: false,
            passwordErrorMsg: '',

        };


        if ((this.state.name.length < 5 || !/^[a-zA-Z]+$/.test(this.state.name))) {
            errors.nameErrorMsg = 'User name need to be Alphabetic and at least 5 characters  ';
            errors.nameError = true;
            Error = true;
        }


        if (!/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/.test(this.state.email)) {
            errors.emailErrorMsg = 'Not Valid email';
            errors.emailError = true;
            Error = true;

        }


        if (this.state.password < 5 || !/^[a-zA-Z0-9]+$/.test(this.state.password)) {
            errors.passwordErrorMsg = 'Password not les then 5 characters alphabetic & numbers ';
            errors.passwordError = true;
            Error = true;

        }


        this.setState(
            errors
        );

        return Error;

    }

    handleSubmit(event) {
        event.preventDefault();
        this.validate();


        if (!this.validate()) {

            this.setState({
                loading: true
            }, () => {


                axios.post('api/register', this.state)
                    .then(response => {


                        this.setState({
                            loading: false
                        }, () => {

                            if (response.data.email !== undefined) {

                                this.setState({
                                    emailErrorMsg: response.data.email,
                                    emailError: true
                                });
                                return false

                            }

                            let data = response.data;
                            let cookies = new Cookies();
                            cookies.set("user", data.token);
                            this.props.history.push('/home');
                        });


                    })

            });


        }


    };


    render() {

        return (
            <div>
                {this.state.loading ? (
                    <div className='spinnerWrapper'>
                        <FontAwesomeIcon className='spinner' icon="spinner"/>
                    </div>
                ) : (
                    <div className='mrg'>
                        <h1>Registration Form</h1>
                        <form className='container' noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                            <Link to={'/'}>Login</Link>

                            <TextField
                                id="standard-name"
                                label="Name"
                                className='textField'
                                onChange={this.handleChange}
                                margin="normal"
                                name="name"
                                value={this.state.name}
                                error={this.state.nameError}
                                helperText={this.state.nameErrorMsg}

                            />
                            <TextField
                                id="filled-email-input"
                                label="Email"
                                className="textField"
                                type="email"
                                value={this.state.email}
                                name="email"
                                onChange={this.handleChange}
                                autoComplete="email"
                                margin="normal"
                                helperText={this.state.emailErrorMsg}
                                error={this.state.emailError}


                            />
                            <TextField
                                id="filled-password-input"
                                label="Password"
                                className='textField'
                                type="password"
                                value={this.state.password}
                                onChange={this.handleChange}
                                autoComplete="current-password"
                                margin="normal"
                                name="password"
                                helperText={this.state.passwordErrorMsg}
                                error={this.state.passwordError}


                            />

                            <Button variant="outlined" type="submit" color="primary"
                                    className='button register-login-btn'>
                                Register
                            </Button>
                        </form>
                    </div>
                )}
            </div>
        );
    }
}



export default Registration;

