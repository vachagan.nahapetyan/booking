import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import {Link,} from "react-router-dom";
import {Cookies} from 'react-cookie';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            emailError: false,
            passwordError: false,
            emailErrorMsg: '',
            passwordErrorMsg: '',
            loading: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validate = this.validate.bind(this);


    }

    validate() {

        let Error = false;
        const errors = {
            nameError: false,
            nameErrorMsg: '',
            emailError: false,
            emailErrorMsg: '',
            passwordError: false,
            passwordErrorMsg: '',

        };


        if (!/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/.test(this.state.email)) {
            errors.emailErrorMsg = 'Not Valid email';
            errors.emailError = true;
            Error = true;

        }


        if (this.state.password < 5 || !/^[a-zA-Z0-9]+$/.test(this.state.password)) {
            errors.passwordErrorMsg = 'Password not les then 5 characters alphabetic & numbers ';
            errors.passwordError = true;
            Error = true;

        }


        this.setState(
            errors
        );

        return Error;

    }


    handleChange(event) {
        const data = {};
        data[event.target.name] = event.target.value;

        this.setState(
            data
        );
    };

    handleSubmit(event) {
        event.preventDefault();
        this.validate();


        if (!this.validate()) {
            this.setState({
                loading: true
            }
            , () => {

                let userData = {
                    password: this.state.password,
                    email: this.state.email
                };

                axios.post('api/login',userData)

                    .then(response => {

                        this.setState({
                            loading: false
                        });

                        let data = response.data;
                        let cookies = new Cookies();
                        cookies.set("user", data.token, {maxAge: 3600});
                        this.props.history.push('/home');

                    });
            });
        }
    };


    render() {

        return (
            <div>
                {this.state.loading ?
                    (<div className='spinnerWrapper'>
                            <FontAwesomeIcon className='spinner' icon="spinner"/>
                        </div>
                    ) : (
                        <div className='mrg'>
                            <h1>Login Form</h1>
                            <form className='container' noValidate autoComplete="off" onSubmit={this.handleSubmit}>
                                <Link to={'/registration'}>Registration</Link>

                                <TextField
                                    id="filled-email-input"
                                    label="Email"
                                    className="textField"
                                    type="email"
                                    value={this.state.email}
                                    name="email"
                                    onChange={this.handleChange}
                                    autoComplete="email"
                                    margin="normal"
                                    helperText={this.state.emailErrorMsg}
                                    error={this.state.emailError}

                                />
                                <TextField
                                    id="filled-password-input"
                                    label="Password"
                                    className='textField'
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.handleChange}
                                    autoComplete="current-password"
                                    margin="normal"
                                    name="password"
                                    helperText={this.state.passwordErrorMsg}
                                    error={this.state.passwordError}
                                />

                                <Button variant="outlined" type="submit" color="primary"
                                        className='button register-login-btn'>
                                    Login
                                </Button>
                            </form>
                        </div>
                    )}

            </div>
        );
    }
}

export default Login;