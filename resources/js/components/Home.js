import React, {Component} from 'react';
import axios from 'axios';
import {Cookies} from 'react-cookie';
import Button from '@material-ui/core/Button';
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {fas} from '@fortawesome/free-solid-svg-icons'
import {faSpinner} from '@fortawesome/free-solid-svg-icons'
import styles from '../css/spinnerAnimation.css'
import Redirect from "react-router-dom"


library.add(fas, faSpinner);


class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: false,
            dataError:false
        };

        this.cook = new Cookies;

        this.config = {
            headers: {
                'Authorization': "Bearer " + this.cook.get('user')
            }
        };
        this.logOut = this.logOut.bind(this);

    }


    logOut() {
        this.cook.remove('user');
        this.props.history.push('/');
    };

    componentWillMount() {

        this.setState({
            loading: true
        }, () => {

            axios.get('api/home', this.config).then(response => {
                let data = response.data;
                this.setState({
                    data,
                    loading:false
                });
            }).catch((error) => {

                if(error.response.status){
                    this.props.history.push(`/`);
                }
            })

        });


    }


    render() {
        return (
            <div>
                {this.state.loading ? (

                    <div className='spinnerWrapper'>
                        <FontAwesomeIcon className='spinner' icon="spinner"/>
                    </div>
                ) : (
                    <ul>
                        Home Page
                        <h1>
                            Welcome  {this.state.data.name}
                        </h1>

                        <Button variant="outlined" onClick={this.logOut} color="primary" className='button'>
                            Log out
                        </Button>

                    </ul>
                )}
            </div>

        )
    }

}


export default Home;