import React, { Component } from 'react';
import Registration from "./Registration";
import Login from './Login'
import Home from './Home'
import { Route,Switch,Redirect} from 'react-router-dom'
import Auth from './Auth';
import NoMatch from "./NoMatch";
import '../css/NotFound.css';
import   "../css/Login&Registration.css"






const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
           Auth.isAuth() ? (<Component {...props} />
            ) : (
              <Redirect to={'/'}/>
            )
        }
    />
);



export default class Main extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route path={'/registration'} component={Registration}/>
                    <PrivateRoute exact path="/home" component={Home} />
                    <Route exact path={'/'} component={Login}/>
                    <Route component={NoMatch}/>

                </Switch>
            </div>
        );
    }
}


