import React,{Component} from 'react';
class NoMatch extends Component {

    constructor(props){
        super(props);

        this.goBack = this.goBack.bind(this)

    }

    goBack(){
        this.props.history.goBack();
    }


    render() {
        return (

            <div className="errors">
                <article className="error-page ">

                        <h1 className='h404'>404</h1>
                    <h1 className='no'>Page Not Found</h1>

                    <button className='bt' onClick={this.goBack}>Go Back</button>
                </article>
            </div>
        )

    }
}

export default NoMatch;